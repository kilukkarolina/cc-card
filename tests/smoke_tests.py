import os
import unittest
from selenium import webdriver


class SmokeTests(unittest.TestCase):
   @classmethod
   def setUpClass(self):
       if os.name == 'nt':
           chromedriver_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), r"libs\chromedriver.exe")
           self.driver = webdriver.Chrome(executable_path=chromedriver_path)
       else:
           chrome_options = webdriver.ChromeOptions()
           chrome_options.add_argument('--no-sandbox')
           chrome_options.add_argument('--headless')
           chrome_options.add_argument('--disable-gpu')
           self.driver = webdriver.Chrome(options=chrome_options)

       self.base_url = 'https://cc-card-staging.herokuapp.com/'
       self.driver.get(self.base_url)

   @classmethod
   def tearDownClass(self):
       self.driver.quit()

   def test_page_header(self):
       expected_name = 'Karolina Kiluk'
       xpath = '//div[@class="intro"]//h2'
       email_element = self.driver.find_element_by_xpath(xpath)
       observed_name = email_element.text
       self.assertEqual(expected_name, observed_name)

   def test_phone_number(self):
       expected_phone_number = '337-4139538'
       xpath = '//*[@class="icon ion-ios-telephone-outline"]/following-sibling::div/h5'
       phone_nb_element = self.driver.find_element_by_xpath(xpath)
       observed_phone_number = phone_nb_element.text
       self.assertEqual(expected_phone_number, observed_phone_number)

   def test_email(self):
       expected_email = 'EMAIL : mymith@mywebpage.com'
       xpath = '//*[text()="EMAIL"]/parent::li'
       email_element = self.driver.find_element_by_xpath(xpath)
       observed_email = email_element.text
       self.assertEqual(expected_email, observed_email)